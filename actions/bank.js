import { get } from '../utils/agent';
import { bankService } from '../utils/endpoint';
import {
    FETCH_BANK_ACCOUNTS_SUCCESS,
} from './actionTypes';

export const fetchAllBankAccounts = () =>
    async (dispatch, getState) => {
        const url = `${bankService}/bank-accounts`;
        try {
            const response = await get(url);
            dispatch(fetchBankAccountsSuccess(response.data));
        }
        catch (err) {
            console.log(err);
        }
    }

const fetchBankAccountsSuccess = (data) => ({ type: FETCH_BANK_ACCOUNTS_SUCCESS, data });