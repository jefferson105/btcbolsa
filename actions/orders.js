import { get, post } from '../utils/agent';
import { ordersService } from '../utils/endpoint';
import {
    FETCH_ALL_ORDERS_SUCCESS,
    FETCH_TICKER_SUCCESS,
    FETCH_TRADES_SUCCESS
} from './actionTypes';

export const fetchAllOrders = () =>
    async (dispatch, getState) => {
        const url = `${ordersService}/my-orders?type=buy&pair=BTC/BRL&status=all&page=0`;
        try {
            const response = await get(url);
            dispatch(fetchAllOrdersSuccess(response.data));
        }
        catch (err) {
            console.log(err);
        }
    }

export const fetchAvailablePairs = () =>
    async (dispatch, getState) => {
        const url = `${ordersService}/available-pairs`;
        try {
            const response = await get(url);
            return response.data;
        }
        catch (err) {
            console.log(err);
        }
    }

export const sendDeposit = (data) =>
    async (dispatch, getState) => {
        const url = `${ordersService}/send-deposit`;
        try {
            await post(url, data);
            return { success: true };
        }
        catch (err) {
            return { success: false, message: 'Erro Interno' };
        }
    }

export const sendReceiptImg = (img) =>
    async (dispatch, getState) => {

        const url = `${ordersService}/upload-deposit`;
        try {
            const formData = new FormData();
            formData.append('file', img);
            const response = await post(url, formData);
            return { success: true, receipt: response.url };
        }
        catch (err) {
            return { success: false, message: 'Erro Interno' };
        }
    }


export const dispatchTicker = (data) =>
    async (dispatch, getState) => {
        dispatch(fetchTickerSuccess(data));
    }

export const dispatchTrades = (data) =>
    async (dispatch, getState) => {
        dispatch(fetchTradesSuccess(data));
    }

const fetchAllOrdersSuccess = (data) => ({ type: FETCH_ALL_ORDERS_SUCCESS, data });

const fetchTickerSuccess = (data) => ({ type: FETCH_TICKER_SUCCESS, data });

const fetchTradesSuccess = (data) => ({ type: FETCH_TRADES_SUCCESS, data });