import { post } from '../utils/agent';
import { withdrawService } from '../utils/endpoint';

export const withdrawMoney = (data) =>
    async (dispatch, getState) => {
        const url = `${withdrawService}/withdraw-money`;
        try {
            await post(url, data);
            return { success: true };
        }
        catch (err) {
            return { success: false, message: 'Erro Interno' };
        }
    }
