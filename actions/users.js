import { get } from "../utils/agent";
import { userService } from "../utils/endpoint";
import { FETCH_USER_PROFILE_SUCCESS, FETCH_MY_BALANCE } from './actionTypes';

export const fetchUserProfile = () =>
    async (dispatch) => {
        try {
            const response = await get(`${userService}/me`);
            dispatch(fetchUserProfileSuccess(response.user));
        }
        catch (err) {

        }
    }

export const fetchMyBalance = () =>
    async (dispatch) => {
        try {
            const response = await get(`${userService}/my-balance`);
            dispatch(fetchMyBalanceSuccess(response.data));
        }
        catch (err) {
            console.log(err);
        }
    }

const fetchUserProfileSuccess = (data) => ({ type: FETCH_USER_PROFILE_SUCCESS, data });
const fetchMyBalanceSuccess = (data) => ({ type: FETCH_MY_BALANCE, data });