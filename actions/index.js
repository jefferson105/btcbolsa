import socket from '../utils/socketConnection';

export const actionTypes = {
    SET_LIVRO_ORDENS: "SET_LIVRO_ORDENS",
    SHOW_MODAL_DEPOSITO: "SHOW_MODAL_DEPOSITO",
    SHOW_MODAL_DEPOSITO_REAL: "SHOW_MODAL_DEPOSITO_REAL",
    SHOW_MODAL_DEPOSITO_CONFIRMACAO: "SHOW_MODAL_DEPOSITO_CONFIRMACAO",
    HIDE_MODAL_DEPOSITO: "HIDE_MODAL_DEPOSITO",
    HIDE_MODAL_DEPOSITO_REAL: "HIDE_MODAL_DEPOSITO_REAL",
    HIDE_MODAL_DEPOSITO_CONFIRMACAO: "HIDE_MODAL_DEPOSITO_CONFIRMACAO",
    SET_BANCO_DEPOSITO: "SET_BANCO_DEPOSITO"
};

export const getLivroOrdens = (data) => {
    return async dispatch => {
        const vendas = data.filter(({ side }) => side == "sell");
        const compras = data.filter(({ side }) => side == "buy");

        dispatch({ type: actionTypes.SET_LIVRO_ORDENS, data: { vendas, compras, todos: data, acumulado: false } });
    }
}

export const acumularOrdens = () => {
    return async (dispatch, getState) => {
        let { livroOrdens } = getState();

        let vendas = livroOrdens.todos.filter(({ side }) => side != "buy");
        let compras = livroOrdens.todos.filter(({ side }) => side == "buy");

        if (!livroOrdens.acumulado) {
            compras = Object.values(compras.reduce((ac, val, i) => {
                let preco = parseFloat(val.price)

                if (!ac[preco]) {
                    ac[preco] = [];
                }

                ac[preco].push(val);

                return ac;
            }, {})).map(v => {
                var amount = v.reduce((acc, val) => {
                    acc += parseFloat(val.amount)

                    return acc;
                }, 0);

                return { ...v[0], amount };
            });
        }

        if (compras.length > vendas.length) {
            vendas = [...vendas, ...Array(Math.abs(compras.length - vendas.length)).map(_ => ({}))];
        }

        dispatch({ type: actionTypes.SET_LIVRO_ORDENS, data: { vendas, compras, todos: livroOrdens.todos, acumulado: !livroOrdens.acumulado } });
    }
}

export const showModalDeposito = () => {
    return { type: actionTypes.SHOW_MODAL_DEPOSITO };
};

export const showModalRealState = () => {
    return { type: actionTypes.SHOW_MODAL_DEPOSITO_REAL };
};

export const showModalConfirmacao = () => {
    return { type: actionTypes.SHOW_MODAL_DEPOSITO_CONFIRMACAO };
};

export const hideModalDeposito = () => {
    return { type: actionTypes.HIDE_MODAL_DEPOSITO };
};

export const hideModalReal = () => {
    return { type: actionTypes.HIDE_MODAL_DEPOSITO_REAL };
};

export const hideModalConfirmacao = () => {
    return { type: actionTypes.HIDE_MODAL_DEPOSITO_CONFIRMACAO };
};

export const setBanco = (data) => {
    return { type: actionTypes.SET_BANCO_DEPOSITO, data };
};