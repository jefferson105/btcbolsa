import Aside from "../components/aside";
import Header from "../components/header";

class Transferencia extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside />
                <main className="main">
                    <Header page="7" />
                    <h1>Tranferência</h1>
                </main>
            </div>
        )
    }
}

export default Transferencia;