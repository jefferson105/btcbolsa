import Document, { Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
    render() {
        return (
            <html>
                <Head>
                    <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
                    <link rel="stylesheet" href="/_next/static/style.css" />
                    <script>
                        TradingView = null; 
                    </script>
                    <script async type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        )
    }
}

//<meta name="viewport" content="width=device-width, initial-scale=1" />