import { connect } from "react-redux";

import AsideTrade from "../components/asideTrade";
import AsidePayment from "../components/paymentTrade";
import Header from "../components/header";

import accounting from "accounting-js";
import moment from 'moment';
import currencyFormatter from 'currency-formatter';
import { getLivroOrdens, acumularOrdens } from "../actions";
import socket from '../utils/socketConnection';

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            allOrders: []
        }
    }

    componentDidMount() {
        if (!!TradingView) {
            new TradingView.widget(
                {
                    "autosize": false,
                    "symbol": "NASDAQ:AAPL",
                    "interval": "D",
                    "timezone": "Etc/UTC",
                    "theme": "Dark",
                    "style": "1",
                    "locale": "br",
                    "toolbar_bg": "#1C1D40",
                    "enable_publishing": false,
                    "allow_symbol_change": true,
                    "container_id": "tradingview_e47e3",
                    "height": "300",
                    "width": "100%"
                }
            );

            this.graph.childNodes[0].childNodes[0].style.width = "100%";
        }

        socket.on('order_book', (data) => {
            this.props.dispatch(getLivroOrdens(data));
        });
    }

    render() {
        const { livroOrdens, trades = [], dispatch } = this.props;

        return (
            <div style={{ display: "flex" }}>
                <AsideTrade />
                <main className="main">
                    <Header />
                    <section style={{ width: "99%", padding: "1rem", marginTop: "1rem", boxSizing: "border-box" }}>
                        <div style={{ height: "300px" }} className="tradingview-widget-container">
                            <div ref={(el) => this.graph = el} id="tradingview_e47e3"></div>
                        </div>
                    </section>
                    <section style={{ width: "99%" }} className="sec-table">
                        <div className="table-main--order">
                            <h3 className="table-main__caption">LIVRO DE ORDENS <button onClick={() => dispatch(acumularOrdens())} className={`table-main__cap-button ${!!livroOrdens.acumulado && "table-main__cap-button--selected"}`}>ACUMULADO</button></h3>
                            <table className="table-main" style={{ borderRight: "thin solid #2F3A53" }}>
                                <tbody>
                                    <tr>
                                        <td colSpan="3">
                                            <table className="table-main__inside table-main__inside--border">
                                                <thead>
                                                    <tr className="table-main__tr">
                                                        <th className="middle">Valor Total</th>
                                                        <th>Preço Unitário</th>
                                                        <th>Quantidade</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        livroOrdens.compras.slice(0, 50).map((info, i) => <TrTableOrder {...info} key={i} />)
                                                    }
                                                </tbody>
                                            </table>
                                        </td>
                                        <td colSpan="3">
                                            <table className="table-main__inside">
                                                <thead>
                                                    <tr className="table-main__tr">
                                                        <th>Quantidade</th>
                                                        <th>Preço Unitário</th>
                                                        <th>Valor Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        livroOrdens.vendas.slice(0, 50).map((info, i) => <TrTableOrder1 {...info} key={i} />)
                                                    }
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="table-main--trades">
                            <h3 className="table-main__caption">
                                NEGÓCIOS
                                <p className="table-main__btns">
                                    <button className="table-main__button table-main__button--active">Mercado</button>
                                    <button className="table-main__button">Minhas</button>
                                </p>
                            </h3>
                            <table className="table-main">
                                <thead>
                                    <tr className="table-main__tr">
                                        <th>Quantidade</th>
                                        <th>Preço unitário</th>
                                        <th>Data/Hora</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        trades.map((data, index) => <TrTableTrade key={index} trade={data} />)
                                    }
                                </tbody>
                            </table>
                        </div>
                    </section>
                </main>
            </div>
        )
    }
}

//<AsidePayment />

const TrTableOrder = ({ amount, currency_from, currency_to, id, price, side }) =>
    <tr>
        <td>{currencyFormatter.format(price * amount, { symbol: "R$", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })}</td>
        <td className="table-main__azul">{currencyFormatter.format(price, { symbol: "R$", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })}</td>
        <td>{currencyFormatter.format(amount, { symbol: "Ƀ", format: "%s %v", decimal: ",", thousand: ".", precision: 8 })}</td>
    </tr>

const TrTableOrder1 = ({ amount, currency_from, currency_to, id, price, side }) =>
    <tr>
        <td>{!amount ? "-" : currencyFormatter.format(amount, { symbol: "Ƀ", format: "%s %v", decimal: ",", thousand: ".", precision: 8 })}</td>
        <td className="table-main__red">{!price ? "-" : currencyFormatter.format(price, { symbol: "R$", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })}</td>
        <td>{!price && !amount ? "-" : currencyFormatter.format(price * amount, { symbol: "R$", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })}</td>
    </tr>

const TrTableTrade = ({ trade }) => {
    return (
        <tr>
            <td>
                {accounting.formatMoney(
                    trade.amount,
                    {
                        symbol: "R$",
                        format: "%s %v",
                        decimal: ",",
                        thousand: ".",
                        precision: 2
                    })}
            </td>
            <td className='table-main__azul'>
                {accounting.formatMoney(
                    trade.price_unity,
                    {
                        symbol: "R$",
                        format: "%s %v",
                        decimal: ",",
                        thousand: ".",
                        precision: 2
                    })}
            </td>
            <td><time>{moment(trade.time_executed).format('DD/MM/YYYY HH:mm')}</time></td>
        </tr>
    )
}

const mapStateToProps = (state) => {
    const { livroOrdens, trades } = state.orders;
    return { livroOrdens, trades }
}
export default connect(mapStateToProps)(Index);