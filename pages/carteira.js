import { connect } from "react-redux";
import { InputNumber, Form, Button, Alert } from 'antd';
import Aside from "../components/asideTrade";
import Header from "../components/header";
import TableSaldo from "../components/tableSaldo";
import ModalDeposito from "../components/carteira/ModalDeposito";
import ModalReal from "../components/carteira/ModalReal";
import ModalConfirmacao from "../components/carteira/ModalConfirmacao";
import { fetchMyBalance } from "../actions/users";
import { sendDeposit, sendReceiptImg } from "../actions/orders";
import accounting from 'accounting-js';
import { qrCodeSrc } from '../utils/qrCode';
import { Icon } from 'antd';
import { Spin } from 'antd';
import ReactTooltip from 'react-tooltip'
import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import { Select } from 'antd';

const Option = Select.Option;

const FormItem = Form.Item;

const cryptoMask = createNumberMask({
  prefix: 'B ',
  suffix: '',
  includeThousandsSeparator: false,
  allowDecimal: true,
  decimalSymbol: ',',
  decimalLimit: 8,
})

const fiatMask = createNumberMask({
  prefix: 'R$ ',
  suffix: '',
  includeThousandsSeparator: true,
  allowDecimal: true,
  decimalSymbol: ',',
  thousandsSeparatorSymbol: '.',
  decimalLimit: 2,
})

class Carteira extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            document: 'Selecione o comprovante',
            showCryptoDeposit: false,
            showModalReal: false, 
            showConfirmation: false,
            currency: {},
            depositError: '',
            depositMessage: '',
            successMessage: '',
            receiptImg: '',
            barWidth: '50%',
            loadingSubmit: false, 
        }
    }

    fixPath(value) {
        return value.replace(/C:\\fakepath\\/, '');
    }

    onChange = (value) => {
        console.log(value);
    }

    hasErrors = (fieldsError) => {
        return Object.keys(fieldsError).some(field => fieldsError[field]);
    }

    handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            amount: 12500.50,
            type: 'TED',
        };

        this.setState({ loadingSubmit: true });
        const response = await this.props.dispatch(sendDeposit(data));

        if (response.success) {
            try {
                await this.props.dispatch(sendReceiptImg(this.state.receiptImg));

                return this.setState({ successMessage: 'Depósito enviado com sucesso!', loadingSubmit: false })
            }

            catch (error) {
                return this.setState({
                    depositError: 'Erro interno',
                });
            }

        }

        this.setState({
            depositError: response.message,
            loadingSubmit: false,
        });
    }

    handleOpenModal = (data) => {
        if (data.currency !== 'BRL') {
            return this.setState({
                showCryptoDeposit: true,
                currency: JSON.parse(JSON.stringify(data)),
                barWidth: 80,
            });
        }

        this.setState({
            showModalWithdrawBRL: true,
            barWidth: 40,
        });
    }

    handleImageChange = (e) => {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        if (file) {
            reader.onloadend = () => {
                this.setState({
                    receiptImg: file,
                });
            };
            reader.readAsDataURL(file);
        }
    }

    copyToClipboard = (e) => {
      document.getElementById(e).select();
      document.getElementById(e).focus();
      document.execCommand('copy');
    };

    handleChange(value) {
      if(value == 'new') {
        // REDIRECIONAR PARA "MINHAS CONTAS BANCÁRIAS"
      }
    }

    async componentDidMount() {
        await this.props.dispatch(fetchMyBalance());
        this.setState({ loading: false })
    }

    render() {
        const {
            loading, showCryptoDeposit, showModalReal, showModalWithdrawBRL,
            showConfirmation, barWidth, currency, depositError, successMessage
        } = this.state;
        const { modalDeposito, dispatch, myBalance: { balance = [] } } = this.props;
        const { getFieldDecorator, getFieldError, getFieldsError, isFieldTouched } = this.props.form;
        // Only show error after a field is touched.
        const amountError = isFieldTouched('amount') && getFieldError('amount');

        return (
            <div style={{ display: "flex" }}>
            
                <Aside />
                <main className="main">
                    <Header page="2" />
                    {
                        loading ? <Spin tip="Por favor, aguarde..."></Spin> :
                            <div className="carteira container-content">
                                <section style={{ marginBottom: "1rem" }}>
                                    <h2 className="carteira__title">SALDO EQUIVALENTE</h2>
                                    <div className="carteira__center">
                                        <p className="carteira__cash"><span>USD</span> <b>0,00</b></p>
                                        <p className="carteira__cash"><span>BTC</span> <b>0,00000000</b></p>
                                    </div>
                                </section>
                                <section style={{ marginBottom: "2rem" }}>
                                    <h3 className="table-strip__caption">MEU SALDO</h3>
                                    {balance.length === 0
                                        ? <h3>Erro ao localizar saldos!</h3>
                                        : <table className="table-saldo">
                                            <thead>
                                                <tr className="table-saldo__tr-header">
                                                    <th>Moeda</th>
                                                    <th>Porcentagem</th>
                                                    <th>Saldo disponível</th>
                                                    <th>Saldo em uso</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {balance.map((data, index) => (
                                                    <tr key={index}>
                                                        <td className="table-saldo__moeda" style={{ textTransform: 'capitalize' }}><span>{data.currency_symbol}</span> {data.currency_name}</td>
                                                        <td>
                                                            <p>85,00%</p>
                                                            <div className="table-saldo__percent"><p style={{ width: "85%" }} className="table-saldo__percent-inner"></p></div>
                                                        </td>
                                                        <td>
                                                            {accounting.formatMoney(data.available_balance, {
                                                                symbol: data.currency_symbol,
                                                                format: "%s %v",
                                                                decimal: ",",
                                                                thousand: ".",
                                                                precision: data.currency_symbol === 'R$' ? 2 : 8,
                                                            })}
                                                        </td>
                                                        <td>
                                                            {accounting.formatMoney(data.hold_balance, {
                                                                symbol: data.currency_symbol,
                                                                format: "%s %v", decimal: ",",
                                                                thousand: ".",
                                                                precision: data.currency_symbol === 'R$' ? 2 : 8,
                                                            })}
                                                        </td>
                                                        <td>
                                                            <button className="table-saldo__button"
                                                                onClick={() => this.handleOpenModal(data)}>
                                                                <Icon type="down" style={{marginRight:'5px'}} /> Depositar
                                                            </button>
                                                            <button className="table-saldo__button"
                                                                onClick={() => this.handleOpenModal(data)}>
                                                                <Icon type="up" style={{marginRight:'5px'}} /> Sacar
                                                            </button>
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>}
                                </section>
                                <TableSaldo />
                            </div>}
                </main>
                {
                    !!showCryptoDeposit &&
                    <div className="screen-overlay">
                        <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
                            <header className="overlay-deposito__header">
                                <h3 style={{ textTransform: 'capitalize' }}>Receber {currency.currency_name}</h3>
                                <button onClick={() => this.setState({ showCryptoDeposit: false })} className="overlay-deposito__close"><img src="/static/img/cancel.svg" /></button>
                            </header>
                            <div className="overlay-deposito__content">
                                <div className="overlay-deposito__container-input">
                                    <label className="overlay-deposito__label">Endereço da Carteira</label>
                                    <p className="overlay-deposito__wallet">
                                        <input id={currency.currency.toLowerCase()+'_address'} value={currency.wallet} style={{ background:'transparent',border:'none',width:'100%',cursor:'default' }} readOnly />
                                        <img data-tip="Copiar endereço de depósito" className="overlay-deposito__copy" src="/static/img/copy.svg" style={{ cursor:'pointer' }} onClick={() => this.copyToClipboard(currency.currency.toLowerCase()+'_address')} />
                                        <ReactTooltip effect={'solid'} place={'right'} border={true} className={'tooltip'} />
                                    </p>
                                </div>
                                <div className="overlay-deposito__info">
                                    <div className="overlay-deposito__container-txt" style={{ width:'100%' }} >
                                        <p style={{ letterSpacing: '0px', lineHeight:'1.5rem' }}>
                                          - <b>Valor mínimo para depósito:</b> {currency.currency_symbol} 0,0001
                                          <br/>
                                          - Seu depósito será liberado após <b>{currency.min_confirmations} confirmações</b> na Blockchain.
                                          <br/>
                                          - Após o número de confirmações necessárias, o valor do depósito será automaticamente liberado na sua conta.
                                          <br/>
                                          - Envie apenas <b>{currency.currency_name}</b> para este endereço.
                                          <br/>
                                          - Depósitos são <b>isentos de quaisquer comissões</b>.
                                        </p>
                                    </div>
                                    <figure className="overlay-deposito__fig-qr-code">
                                        <img className="overlay-deposito__qr-code"
                                            src={qrCodeSrc(currency.currency_name, currency.wallet)}
                                        />
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                {
                    !!showModalReal &&
                    <div onClick={() => this.setState({ showModalReal: false })} className="screen-overlay">
                        <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
                            <header className="overlay-deposito__header">
                                <h3>Depósito em Reais</h3>
                                <button onClick={() => this.setState({ showModalReal: false })} className="overlay-deposito__close">
                                    <img src="/static/img/cancel.svg" />
                                </button>
                            </header>
                            <div className="overlay-deposito__content">
                                <p className="overlay-deposito__label">Para qual banco deseja realizar o <strong>depósito</strong>?</p>
                                <ul className="overlay-deposito__banks">
                                    <li className="overlay-deposito__banks__item overlay-deposito__banks__item--santander selected">
                                        <span></span>
                                    </li>
                                    <li className="overlay-deposito__banks__item overlay-deposito__banks__item--itau">
                                        <span></span>
                                    </li>
                                    <li className="overlay-deposito__banks__item overlay-deposito__banks__item--caixa">
                                        <span></span>
                                    </li>
                                    <li className="overlay-deposito__banks__item overlay-deposito__banks__item--bradesco">
                                        <span></span>
                                    </li>
                                    <li className="overlay-deposito__banks__item overlay-deposito__banks__item--others">
                                        <p>Outros</p>
                                    </li>
                                </ul>
                                <div className="overlay-deposito__container-input">
                                    <label className="overlay-deposito__label">Como esse depósito para o <strong>Bradesco</strong> será feito?</label>
                                    <div className="columns overlay-deposito__type">
                                        <div className="column overlay-deposito__type__item">TED</div>
                                        <div className="column overlay-deposito__type__item overlay-deposito__type__item--selected">DOC</div>
                                        <div className="column overlay-deposito__type__item">Depósito em Dinheiro</div>
                                    </div>
                                </div>
                                <div className="overlay-deposito__container-input">
                                    <label className="overlay-deposito__label">Qual o valor do depósito?</label>

                                    <MaskedInput mask={fiatMask} placeholder={'R$ 0,00'} className="primary-field overlay-deposito__input overlay-deposito__value" placeholder="R$ 0,00" />

                                    <div className="overlay-deposito__bar">
                                        <div className="overlay-deposito__bar__container">
                                            <span className="overlay-deposito__bar__limit">Limite</span>
                                            <span className="overlay-deposito__bar__value">R$ 2.000,00</span>
                                        </div>
                                        <div className="overlay-deposito__bar__current" style={{ width: barWidth }}></div>
                                        <div className="overlay-deposito__bar__help">!</div>
                                    </div>
                                    <a href="#" className="overlay-deposito__bar__increase">Deseja aumentar seu limite?</a>
                                </div>
                            </div>
                            <footer className="overlay-deposito__footer"> 
                                <button onClick={() => this.setState({ showReal: false })} className="primary-button primary-button--text">Cancelar</button>
                                <Button
                                    onClick={() => this.setState({ showReal: false, showConfirmation: true })}
                                    type="primary"
                                    className="primary-button primary-button--continue"
                                    disabled={this.hasErrors(getFieldsError())}>
                                    Continuar
                                </Button> 
                            </footer>
                        </div>
                    </div>
                }

                {
                    !!showConfirmation &&
                    <div onClick={() => this.setState({ showConfirmation: false })} className="screen-overlay">
                        <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
                            <header className="overlay-deposito__header">
                                <h3>Confirmação do Depósito</h3>
                                <button onClick={() => this.setState({ showConfirmation: false })} className="overlay-deposito__close">
                                    <img src="/static/img/cancel.svg" />
                                </button>
                            </header>
                            <div className="overlay-deposito__content">
                                <div className="overlay-deposito__block">
                                    <div className="overlay-deposito__block__item">
                                        <h4 className="overlay-deposito__block__name">Dados do Depósito</h4>
                                        <ul className="overlay-deposito__block__list">
                                            <li>
                                                <span>Titular</span>
                                                {Object.keys(this.props.profile).length > 0 && this.props.profile.name}
                                            </li>
                                            <li><span>Banco</span> Banco Bradesco SA.</li>
                                            <li><span>Tipo</span> Transferência entre contas</li>
                                            <li><span>Valor</span> R$ 2.000,00</li>
                                        </ul>
                                    </div>
                                    <div className="overlay-deposito__block__item">
                                        <h4 className="overlay-deposito__block__name">Dados do Destinatário</h4>
                                        <ul className="overlay-deposito__block__list">
                                            <li><span>Titular</span> Noctacoin Intermediações LTDA</li>
                                            <li><span>CNPJ</span> 28.146.526/0001-01</li>
                                            <li><span>Banco</span> Banco Bradesco S.A.</li>
                                        </ul>
                                        <ul className="overlay-deposito__block__list overlay-deposito__block__list--inline">
                                            <li><span>Agência</span> 1236-0</li>
                                            <li><span>Conta</span> 0608367-6</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="overlay-deposito__warning">
                                    <h4 className="overlay-deposito__warning__title">Dados do Depósito</h4>
                                    <ul className="overlay-deposito__warning__list">
                                        <li>• Lembre-se de <strong>conferir seus dados</strong> para evitar cancelamentos ou atrasos.</li>
                                        <li>• Obrigatoriamente, este depósito deve vir de uma conta pertencente a <strong>Wallace Erick da Silva</strong> - 372.858.658-77. Caso contrário, ele será cancelado em até <strong>30 dias</strong>.</li>
                                    </ul>
                                </div>
                                <div className="overlay-deposito__files">
                                    <h4 className="overlay-deposito__files__title">Comprovante</h4>
                                    <div className="input-file-container">
                                        <input
                                            className="input-file js-input-file"
                                            id="comprovante"
                                            type="file"
                                            onChange={(e) => this.setState({ document: this.fixPath(e.target.value) }) } />
                                        <div className="file-return">{ this.state.document }</div> 
                                        <label htmlFor="comprovante" className="primary-button primary-button--upload input-file-trigger js-input-file-trigger">Selecionar</label>
                                    </div>
                                </div>

                                <div style={{ paddingTop: '16px' }}></div>

                                {successMessage &&
                                    <Alert
                                        message={successMessage}
                                        type="success"
                                        closeText="Fechar"
                                        onClose={() => this.setState({ successMessage: '' })}
                                    />
                                }

                                {depositError &&
                                    <Alert
                                        message={depositError}
                                        type="error"
                                        closeText="Fechar"
                                        onClose={() => this.setState({ depositError: '' })}
                                    />
                                }
                            </div>
                            <footer className="overlay-deposito__footer">
                                <button onClick={() => this.setState({ showReal: true, showConfirmation: false })} className="primary-button primary-button--text">Voltar</button>
                                <Button
                                    onClick={(e) => this.handleSubmit(e)}
                                    className="primary-button primary-button--continue"
                                    type="primary"
                                    loading={this.state.loadingSubmit}>
                                    Enviar
                                </Button>
                            </footer>
                        </div>
                    </div>
                }

                {
                    !!showModalWithdrawBRL &&
                    <div className="screen-overlay">
                        <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
                            <header className="overlay-deposito__header">
                                <h3>Retirada de Reais para conta bancária</h3>
                                <button onClick={() => this.setState({ showModalWithdrawBRL: false })} className="overlay-deposito__close">
                                    <img src="/static/img/cancel.svg" />
                                </button>
                            </header>
                            <div className="overlay-deposito__content">
                                <p className="overlay-deposito__label">Em qual conta bancária sua você deseja <b>receber o saque</b>?</p>

                                <div className="overlay-deposito__container-input" style={{ marginBottom: '15px' }} >
                                  <Select
                                    showSearch
                                    style={{ width: '100%' }}
                                    theme="dark"
                                    placeholder="Selecione sua conta bancária"
                                    optionFilterProp="children"
                                    onChange={this.handleChange}
                                    onFocus={this.handleFocus}
                                    onBlur={this.handleBlur}
                                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                    >
                                    <Option value="new">Cadastrar nova conta bancária</Option>
                                    <Option value="jack">[C/C] Banco do Brasil - Ag. 0913 - Conta 13000917-2</Option>
                                    <Option value="lucy">Lucy</Option>
                                    <Option value="tom">Tom</Option>
                                  </Select>
                                </div>

                                <div className="overlay-deposito__container-input" style={{display:'inline-block',width:'33%'}}>
                                    <label className="overlay-deposito__label">Qual o valor bruto do saque?</label>
                                    <MaskedInput mask={fiatMask} placeholder={'R$ 0,00'} className="primary-field overlay-deposito__input overlay-deposito__value" placeholder="R$ 0,00" />
                                </div>

                                <div className="overlay-deposito__container-input" style={{display:'inline-block',width:'30%',marginLeft:'2%'}}>
                                    <label className="overlay-deposito__label">Comissão (0,95%):</label>
                                    <input type="text" className="primary-field overlay-deposito__input overlay-deposito__value" value="R$ 1.000,00" style={{ color:'#000' }} readOnly />
                                </div>

                                <div className="overlay-deposito__container-input" style={{display:'inline-block',width:'30%',marginLeft:'2%'}}>
                                    <label className="overlay-deposito__label">Valor líquido (a receber):</label>
                                    <MaskedInput mask={fiatMask} placeholder={'R$ 0,00'} className="primary-field overlay-deposito__input overlay-deposito__value" placeholder="R$ 0,00" />
                                </div>

                                <div className="overlay-deposito__bar">
                                    <div className="overlay-deposito__bar__container">
                                        <span className="overlay-deposito__bar__limit">Limite</span>
                                        <span className="overlay-deposito__bar__value">R$ 2.000,00</span>
                                    </div>
                                    <div className="overlay-deposito__bar__current" style={{ width: barWidth }}></div>
                                    <div className="overlay-deposito__bar__help">!</div>
                                </div>
                                <a href="#" className="overlay-deposito__bar__increase">Deseja aumentar seu limite?</a>

                            </div>
                            <footer className="overlay-deposito__footer">
                                <button onClick={() => this.setState({ showReal: false })} className="primary-button primary-button--text">Cancelar</button>
                                <button onClick={() => this.setState({ showReal: false, showConfirmation: true })} className="primary-button primary-button--continue">Solicitar retirada</button>
                            </footer>
                        </div>
                    </div>
                }

            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    profile: state.users.userProfile,
    myBalance: state.users.myBalance,
});
const WrappedCarteira = Form.create()(Carteira);
export default connect(mapStateToProps)(WrappedCarteira);

//<p className="carteira__cash"><b>258.65</b> <span>CRD</span></p>
//<button className="carteira__sigla">C</button>

/*
<div className="carteira__center">
    <button className="carteira__sigla">R$</button>
    <button className="carteira__sigla">Ƀ</button>
</div>
*/
