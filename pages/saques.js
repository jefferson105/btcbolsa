import Aside from "../components/aside";
import Header from "../components/header";

class Saques extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside />
                <main className="main">
                    <Header page="4" />
                    <h1>Saques</h1>
                </main>
            </div>
        )
    }
}

export default Saques;