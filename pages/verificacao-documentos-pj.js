import Link from "next/link";
import Aside from "../components/aside";
import Header from "../components/header";

class VerifyDocument extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            contract: 'Selecione o contrato',
            card: 'Selecione o cartão',
            photo: 'Foto',
            showSelfie: true,
            showPreviewContract: false,
            previewContract: false
        }
    }

    fixPath(value) {
        return value.replace(/C:\\fakepath\\/, '');
    }

    render() {
        const { showSelfie, showPreviewContract, previewContract } = this.state;
        return (
            <div style={{ display: "flex" }}>
                <Aside page={20} />
                <main className="main">
                    <Header page="7" />
                    <div className="container-content verificacao">
                        <div className="page-title">
                            <h1 className="page-title__name">
                                <Link prefetch href="/verificacao">Verificação de Conta</Link>
                                <span>Documentos</span>
                            </h1>
                            <button className="primary-button primary-button--header">Salvar</button>
                        </div>
                        <div className="verificacao__content verificacao__content--inside">
                            <h2 className="verificacao__item__title">Envio de Documentos <span>Pessoa Jurídica</span></h2>
                            <form className="verificacao__form">
                                <div className="verificacao__input">
                                    <label className="verificacao__input__label">
                                        <span>Contrato Social feferido pela Junta Comercial</span> 
                                        <small>Formatos permitidos: .jpg, .jpeg, .png e .pdf</small>
                                    </label>
                                    <div>
                                        <div className="input-file-container">  
                                            <input 
                                                className="input-file js-input-file" 
                                                id="documento" 
                                                type="file"
                                                onChange={(e) => this.setState({ contract: this.fixPath(e.target.value), showPreviewContract: true })}  />
                                            <div className="file-return">{ this.state.contract }</div>
                                            <label htmlFor="documento" className="primary-button primary-button--upload input-file-trigger">Selecionar</label>
                                        </div>
                                        { !!showPreviewContract && <a href="#" onClick={() => this.setState({ previewContract: true })} className="verificacao__view-document">Visualizar Documento</a> }
                                    </div>
                                </div>
                                <div className="verificacao__input">
                                    <label className="verificacao__input__label">
                                        <span>Cartão CNPJ</span> 
                                        <small>Formatos permitidos: .jpg, .jpeg, .png e .pdf</small>
                                    </label>
                                    <div>
                                        <div className="input-file-container">  
                                            <input 
                                                className="input-file js-input-file" 
                                                id="documento" 
                                                type="file"
                                                onChange={(e) => this.setState({ card: this.fixPath(e.target.value), showPreviewContract: true })}  />
                                            <div className="file-return">{ this.state.card }</div>
                                            <label htmlFor="documento" className="primary-button primary-button--upload input-file-trigger">Selecionar</label>
                                        </div>
                                        { !!showPreviewContract && <a href="#" onClick={() => this.setState({ showPreviewContract: true })} className="verificacao__view-document">Visualizar Documento</a> }
                                    </div>
                                </div>
                                {
                                    !!showSelfie &&
                                    <div className="verificacao__input verificacao__input--selfie">
                                        <label className="verificacao__input__label">
                                            <span>Selfie</span> 
                                            <small>O representante legal da empresa deve enviar <br/>uma selfie segurando uma <strong>folha de papel <br/>com o código abaixo</strong>, assim como mostrado <br/>na imagem ao lado. </small>
                                            <div className="verificacao__input__code">4008</div>
                                            <div className="verificacao__drop" htmlFor="selfie">  
                                                <input id="selfie" onChange={(e) => this.setState({ photo: this.fixPath(e.target.value) })} type="file" />
                                                <div className="verificacao__drop__return"><span>{ this.state.photo }</span></div>
                                            </div>
                                        </label>
                                        <div>
                                            <div className="verificacao__input__selfie">
                                                <img src="https://via.placeholder.com/560x210/fff" />
                                            </div>
                                        </div>
                                    </div>
                                }
                            </form>
                        </div>
                    </div>

                    { !!previewContract &&
                        <div onClick={() => this.setState({ previewContract: false })} className="screen-overlay">
                            <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
                                <header className="overlay-deposito__header">
                                    <h3>Contrato</h3>
                                    <button onClick={() => this.setState({ previewContract: false })} className="overlay-deposito__close">
                                        <img src="/static/img/cancel.svg" />
                                    </button>
                                </header>
                                <div className="overlay-deposito__content">
                                    <p className="overlay-deposito__label">Verifique e confirme a visualização de seu <strong>contrato</strong>.</p>
                                    <div className="verificacao__preview">
                                        <img src="https://via.placeholder.com/560x380/fff" />
                                    </div>
                                </div>
                                <footer className="overlay-deposito__footer">
                                    <button onClick={() => this.setState({ previewContract: false })} className="primary-button primary-button--text">Fechar</button>
                                    <button onClick={() => this.setState({ previewContract: false })} className="primary-button primary-button--continue">OK</button>
                                </footer>
                            </div>
                        </div>
                    }

                    <div className="verificacao__actions">
                        <Link prefetch href="/verificacao">
                            <a href="#" className="primary-button primary-button--continue verificacao__item__button">Voltar</a>
                        </Link>
                    </div>
                </main>
            </div>
        )
    }
}

export default VerifyDocument;