import Link from "next/link";
import Aside from "../components/aside";
import Header from "../components/header"; 

class VerifyPersonal extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {

        // let fileInput  = document.querySelector('.js-input-file'),  
        //     button     = document.querySelector('.js-input-file-trigger'),
        //     the_return = document.querySelector('.js-file-return');
              
        // button.addEventListener('keydown', function( event ) {  
        //     if ( event.keyCode == 13 || event.keyCode == 32 ) {  
        //         fileInput.focus();
        //     }
        // });

        // button.addEventListener('click', function( event ) {
        //     fileInput.focus();
        //     return false;
        // });

        // fileInput.addEventListener('change', function( event ) {  
        //     the_return.innerHTML = this.value;
        // });
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside page={20} />
                <main className="main">
                    <Header page="7" />
                    <div className="container-content verificacao">
                        <div className="page-title">
                            <h1 className="page-title__name">
                                <Link prefetch href="/verificacao">Verificação de Conta</Link>
                                <span>Endereço</span>
                            </h1>
                            <button className="primary-button primary-button--header">Salvar</button>
                        </div>
                        <div className="verificacao__content verificacao__content--inside">
                            <h2 className="verificacao__item__title">Cadastro de Endereço</h2>
                            <form className="verificacao__form">
                                <div className="verificacao__input">
                                    <label className="verificacao__input__label">
                                        <span>CEP</span> 
                                        <small>Busque seu endereço pelo CEP</small>
                                    </label>
                                    <div>
                                        <input className="primary-field primary-field--light verificacao__input__large" type="text" defaultValue="07700-115" placehoder="CEP" />
                                    </div>
                                </div>
                                <div className="verificacao__input">
                                    <label className="verificacao__input__label">
                                        <span>Endereço</span> 
                                        <small>Complete as informações de seu endereço</small>
                                    </label>
                                    <div>
                                        <input className="primary-field primary-field--light verificacao__input__large" type="text" defaultValue="Av. Presidente Kennedy, 616" placehoder="Endereço" /><br/>
                                        <input className="primary-field primary-field--light verificacao__input__loc" type="text" placeholder="Número" placehoder="Número" />
                                        <input className="primary-field primary-field--light verificacao__input__comp" type="text" placeholder="Complemento" placehoder="Complemento" /><br/>
                                        <input className="primary-field primary-field--light verificacao__input__large" type="text" defaultValue="JD. São Francisco" placehoder="Bairro" /><br/>
                                        <input className="primary-field primary-field--light verificacao__input__city" type="text" defaultValue="Caieiras" placehoder="Cidade" />
                                        <input className="primary-field primary-field--light verificacao__input__state" type="text" defaultValue="SP" placehoder="Estado" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div className="verificacao__actions">
                        <Link prefetch href="/verificacao">
                            <a href="#" className="primary-button primary-button--continue verificacao__item__button">Voltar</a>
                        </Link>
                    </div>
                </main>
            </div>
        )
    }
}

export default VerifyPersonal;