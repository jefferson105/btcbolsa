import Aside from "../components/aside";
import Header from "../components/header";

class Depositos extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside page={1} />
                <main className="main">
                    <Header page="8" />
                    <div className="container-content">
                        <div className="seguranca">
                            <h1 className="seguranca__h1">PERFIL DE USUÁRIO</h1>
                            <button className="seguranca__save">SALVAR MUDANÇAS</button>
                        </div>
                        <div className="seguranca__internal-content">
                            <div className="seguranca__content-flex">
                                <div>
                                    <h4>Foto de perfil</h4>
                                    <p>Tamanho máximo 20mb</p>
                                    <button style={{ marginTop: "1rem" }} className="seguranca__button">MUDAR FOTO</button>
                                </div>
                                <figure className="perfil__fig">
                                    <img className="perfil__fig-img" src="/static/img/user-pic.png" />
                                </figure>
                            </div>
                            <div className="seguranca__content-flex">
                                <div>
                                    <h4>Nome de usuário</h4>
                                    <p>Nome que aparecerá em seu perfil público</p>
                                </div>
                                <input type="text" className="seguranca__input" defaultValue="John Johnson" />
                            </div>
                            <div className="seguranca__content-flex">
                                <h4>Email</h4>
                                <input type="email" className="seguranca__input" defaultValue="email@email.com" />
                            </div>
                        </div>
                        <div className="seguranca">
                            <div>
                                <h1 className="seguranca__h1">INFORMAÇÃO PESSOAL</h1>
                                <p>Informações pessoais</p>
                            </div>
                        </div>
                        <div className="seguranca__internal-content">
                            <div className="seguranca__content-flex">
                                <h4>Nome Legal</h4>
                                <form className="perfil__form">
                                    <input defaultValue="John" className="seguranca__input perfil__input" /> 
                                    <input defaultValue="Johnson" className="seguranca__input perfil__input" />
                                </form>
                            </div>
                            <div className="seguranca__content-flex">
                                <h4>Data de nascimento</h4>
                                <form className="perfil__form">
                                    <select className="seguranca__input perfil__input">
                                        <option>Mês</option>
                                    </select>
                                    <select className="seguranca__input perfil__input">
                                        <option>Dia</option>
                                    </select>
                                    <select className="seguranca__input perfil__input">
                                        <option>Ano</option>
                                    </select>
                                </form>
                            </div>
                            <div className="seguranca__content-flex">
                                <h4>Endereço</h4>
                                <form className="perfil__form">
                                    <input style={{ display: "block", width: "100%" }} defaultValue="Endereço 1" className="seguranca__input perfil__input" /> 
                                    <input style={{ display: "block", marginTop: ".5rem" }} defaultValue="Endereço 2" className="seguranca__input perfil__input" />
                                </form>
                            </div>
                            <div className="seguranca__content-flex">
                                <h4>Cidade</h4>
                                <form className="perfil__form">
                                    <input defaultValue="Cidade" className="seguranca__input perfil__input" /> 
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        )
    }
}

export default Depositos;