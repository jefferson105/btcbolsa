import Aside from "../components/aside";
import Header from "../components/header";

class Depositos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: null
        }

        this.bancos = [
            {
                icon: "/static/img/banco-do-brasil.svg",
                nome: "Banco do Brasil",
                slug: "banco-do-brasil",
                agencia: "0000-0",
                conta: "00000-0"
            },
            {
                icon: "/static/img/bradesco-logo.svg",
                nome: "Bradesco",
                slug: "bradesco",
                agencia: "0000-0",
                conta: "00000-0"
            },
            {
                icon: "/static/img/caixa-logo.svg",
                nome: "Caixa",
                slug: "caixa",
                agencia: "0000-0",
                conta: "00000-0"
            },
            {
                icon: "/static/img/santander-logo.svg",
                nome: "Santander",
                slug: "santander",
                agencia: "0000-0",
                conta: "00000-0"
            }
        ]
    }

    render() {
        const { showModal } = this.state;
        return (
            <div style={{ display: "flex" }}>
                <Aside page={4} />
                <main className="main">
                    <Header page={8} />
                    <div className="container-content">
                        <div className="seguranca">
                            <h1 className="seguranca__h1">CONTAS BANCÁRIAS</h1>
                        </div>
                        <div>
                            <ul className="pagamento__list">
                                {
                                    this.bancos.map((p, i) => 
                                        <li onClick={() => this.setState({ showModal: "editar" })} className={'pagamento__item ' + p.slug}>
                                            <h3 className="pagamento__name">{p.nome}</h3>
                                        {/* <li onClick={() => this.setState({ showModal: "editar" })} style={{ backgroundColor: p.bgColor }} className="pagamento__item"> */}  
                                            <div className="pagamento__infos">
                                                <div className="pagamento__image"></div>
                                                <div className="pagamento__account">
                                                    <p><strong>Agência</strong> {p.agencia}</p>
                                                    <p><strong>Conta</strong> {p.conta}</p>
                                                </div>
                                            </div>
                                        </li>
                                    )
                                }
                                
                                <li onClick={() => this.setState({ showModal: "novo" })} className="pagamento__item pagamento__item-add">
                                    <img src="/static/img/plus.svg" />
                                </li>
                            </ul>    
                        </div>
                    </div>
                </main>
                {
                    !!showModal &&
                    <div onClick={() => this.setState({ showModal: null })} className="screen-overlay">
                        <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
                            <header className="overlay-deposito__header">
                                <h3>{ showModal == "novo" ? "Nova Conta" : "Editar Conta" }</h3>
                                <button onClick={() => this.setState({ showModal: null })} className="overlay-deposito__close"><img src="/static/img/cancel.svg" /></button>
                            </header>
                            <div className="overlay-deposito__content">
                                {
                                    !!(showModal == "novo") &&
                                    <React.Fragment>
                                        <label className="overlay-deposito__label">Banco</label>
                                        <select className="overlay-deposito__input">
                                            <option>001 - Banco do Brasil S.A.</option>
                                            <option>237 - Banco Bradesco S.A.</option>
                                            <option>104 - Caixa Econômica Federal</option>
                                            <option>033 - Banco Santander (Brasil) S.A.</option>
                                        </select>
                                    </React.Fragment>
                                }
                                <p className="overlay-deposito__p">
                                    <label className="overlay-deposito__label">Agência</label>
                                    <input className="overlay-deposito__input" />
                                </p>
                                <p className="overlay-deposito__p">
                                    <label className="overlay-deposito__label">Conta</label>
                                    <input className="overlay-deposito__input" />
                                </p>
                                <p className="overlay-deposito__p">
                                    <label className="overlay-deposito__label">Tipo de conta</label>
                                    <select className="overlay-deposito__input">
                                        <option>Conta Corrente</option>
                                        <option>Conta Poupança</option>
                                    </select>
                                </p>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default Depositos;