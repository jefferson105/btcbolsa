import Aside from "../components/aside";
import Header from "../components/header";

class Depositos extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside page={3} />
                <main className="main">
                    <Header page="8" />
                    <div className="container-content">
                        <div className="seguranca">
                            <h1 className="seguranca__h1">SEGURANÇA</h1>
                            <button className="seguranca__save">SALVAR MUDANÇAS</button>
                        </div>
                        <form className="seguranca__internal-content">
                            <div className="seguranca__content-flex">
                                <p>
                                    <label className="seguranca__label">Password</label>
                                    <button className="seguranca__button">MUDAR SENHA</button>
                                </p>
                                <p>
                                    <input className="seguranca__input" type="password" defaultValue="******" />
                                </p>
                            </div>
                            <div className="seguranca__content-flex">
                                <p>
                                    <label className="seguranca__label">Número de telefone</label>
                                    <button className="seguranca__button">VERIFICAR</button>
                                </p>
                                <p style={{ position: "relative" }}>
                                    <input className="seguranca__input" defaultValue="+1 *** *** **56" type="text" />
                                    <button className="seguranca__inside-input">CHANGE</button>
                                </p>
                            </div>
                            <div className="seguranca__content-flex">
                                <div>
                                    <h4 style={{ marginBottom: ".5rem" }}>Autenticação</h4>
                                    <p style={{ width: "15rem" }}>consectetur adipiscing elit. Maecenas mi diam, pretium a est et</p>
                                </div>
                                <p><button style={{ marginRight: "1.9rem" }} className="seguranca__button">HABILITAR AUTENTICAÇÃO</button></p>
                            </div>
                        </form>
                        <section style={{ marginTop: "2rem" }}>
                            <h3 className="seguranca__title">SESSÕES ATIVAS</h3>
                            <table className="table-strip__table"> 
                                <tr className="table-strip__tr">
                                    <th>Login</th>
                                    <th>Dispositivo</th>
                                    <th>Navegador</th>
                                    <th>IP</th>
                                </tr>
                                <tr>
                                    <td>10 minutos atrás</td>
                                    <td>Mac</td>
                                    <td>Safari</td>
                                    <td>100.256.258.456</td>
                                </tr>
                                <tr>
                                    <td>10 minutos atrás</td>
                                    <td>Mac</td>
                                    <td>Safari</td>
                                    <td>100.256.258.456</td>
                                </tr>
                                <tr>
                                    <td>10 minutos atrás</td>
                                    <td>Mac</td>
                                    <td>Safari</td>
                                    <td>100.256.258.456</td>
                                </tr>
                                <tr>
                                    <td>10 minutos atrás</td>
                                    <td>Mac</td>
                                    <td>Safari</td>
                                    <td>100.256.258.456</td>
                                </tr>
                            </table>
                        </section>
                    </div>
                </main>
            </div>
        )
    }
}

export default Depositos;