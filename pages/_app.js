import App, { Container } from "next/app";
import { connect, Provider } from "react-redux";
import withRedux from "next-redux-wrapper";
import Head from 'next/head'
import 'moment/locale/pt-br'; // moment pt-br
import { configureStore } from "../store";

import Loading from "../components/loading";

import '../scss/style.scss';
import '../static/antd-imports';

class MyApp extends App {
    constructor(props) {
        super(props);

        this.state = {
            loading: false
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ loading: false });
        }, 3000)
    }

    render() {
        const { Component, pageProps, store } = this.props;
        const { loading } = this.state;
        return (
            <Container>
                <Provider store={store}>
                    <React.Fragment>
                        <Head>
                            <title>BTC Bolsa</title>
                        </Head>
                        <Component {...pageProps} />
                    </React.Fragment>
                </Provider>
                {
                    !!loading &&
                    <Loading />
                }
            </Container>
        );
    }
}

export default withRedux(configureStore)(connect(state => state)(MyApp));