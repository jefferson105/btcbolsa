import Aside from "../components/aside";
import Header from "../components/header";

class Depositos extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside />
                <main className="main">
                    <Header page="5" />
                    <h1>Depositós</h1>
                </main>
            </div>
        )
    }
}

export default Depositos;