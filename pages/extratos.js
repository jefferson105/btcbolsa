import Aside from "../components/aside";
import Header from "../components/header";

class Extratos extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside />
                <main className="main">
                    <Header page="6" />
                    <h1>Extratos</h1>
                </main>
            </div>
        )
    }
}

export default Extratos;