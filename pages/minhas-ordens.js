import Aside from "../components/asideTrade";
import Header from "../components/header";
import TableOrders from "../components/orders/tableOrders";

class Index extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside />
                <main className="main">
                    <Header page="10" />
                    <div className="carteira container-content">
                        <section style={{ marginBottom: "1rem" }}>
                            <h2 className="carteira__title">MINHAS ORDENS</h2>
                        </section>
                        <section style={{ marginBottom: "2rem" }}>
                            {/* <h3 className="table-strip__caption">MEU SALDO</h3> */}

                            <TableOrders />
                        </section>
                    </div>
                </main>
            </div>
        )
    }
}

export default Index;