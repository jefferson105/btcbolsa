import Link from "next/link";
import Aside from "../components/aside";
import Header from "../components/header"; 

class VerifyPersonal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{ display: "flex" }}>
                <Aside page={20} />
                <main className="main">
                    <Header page="7" />
                    <div className="container-content verificacao">
                        <div className="page-title">
                            <h1 className="page-title__name">
                                <Link prefetch href="/verificacao">Verificação de Conta</Link>
                                <span>Pessoal</span>
                            </h1>
                            <button className="primary-button primary-button--header">Salvar</button>
                        </div>
                        <div className="verificacao__content verificacao__content--inside">
                            <h2 className="verificacao__item__title">Informações Pessoais</h2>
                            <form className="verificacao__form">
                                <div className="verificacao__input">
                                    <label className="verificacao__input__label">
                                        <span>Nome</span> 
                                        <small>Digite seu nome completo</small>
                                    </label>
                                    <div>
                                        <input className="primary-field primary-field--light verificacao__input__large" type="text" defaultValue="Wallace Erick da Silva" />
                                    </div>
                                </div>
                                <div className="verificacao__input">
                                    <label className="verificacao__input__label">
                                        <span>CPF</span> 
                                        <small>Seu documento de identificação</small>
                                    </label>
                                    <div>
                                        <input className="primary-field primary-field--light verificacao__input__large" type="text" defaultValue="372.858.658-77" />
                                    </div>
                                </div>
                                <div className="verificacao__input">
                                    <label className="verificacao__input__label">
                                        <span>Data de Nascimento</span> 
                                        <small>Insira o dia, mês e o ano de seu nascimento</small>
                                    </label>
                                    <div>
                                        <select className="primary-field primary-field--light verificacao__input__date">
                                            <option value="1">09</option>
                                        </select>
                                        <select className="primary-field primary-field--light primary-field--center verificacao__input__date">
                                            <option value="1">Novembro</option>
                                        </select>
                                        <select className="primary-field primary-field--light verificacao__input__date">
                                            <option value="1">1989</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="verificacao__input">
                                    <label className="verificacao__input__label">
                                        <span>Telefone</span> 
                                        <small>Seu telefone de uso pessoal</small>
                                    </label>
                                    <div>  
                                        <input className="primary-field primary-field--light primary-field--first verificacao__input__country" type="text" defaultValue="+55" />
                                        <input className="primary-field primary-field--light verificacao__input__number" type="text" defaultValue="(11) 99983-4963" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div className="verificacao__actions">
                        <Link prefetch href="/verificacao">
                            <a href="#" className="primary-button primary-button--continue verificacao__item__button">Voltar</a>
                        </Link>
                    </div>
                </main>
            </div>
        )
    }
}

export default VerifyPersonal;