import Aside from "../components/aside";
import Header from "../components/header";
import TableSaldo from "../components/tableSaldo";

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search: "",
            showOpt: false
        }

        this.moedas = ["[BTC] Bitcoint", "[RL] Real"];
    }

    componentDidMount() {
        if (!!TradingView) {
            new TradingView.widget(
                {
                    "autosize": false,
                    "symbol": "NASDAQ:AAPL",
                    "interval": "D",
                    "timezone": "Etc/UTC",
                    "theme": "Dark",
                    "style": "1",
                    "locale": "br",
                    "toolbar_bg": "#1C1D40",
                    "enable_publishing": false,
                    "allow_symbol_change": true,
                    "container_id": "tradingview_e47e3",
                    "height": "400",
                    "width": "99.95%"
                }
            );

            this.graph.childNodes[0].childNodes[0].style.width = "100%";
        }
    }

    render() {
        const { search, showOpt } = this.state;

        const moedas = this.moedas.filter((v) => v.toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) > -1);

        console.log(search)

        return (
            <div style={{ display: "flex" }}>
                <Aside />
                <main className="main">
                    <Header page="3" />

                    <div className="grafico">
                        <header className="grafico__header">
                            <div className="grafico__container-select">
                                <input 
                                    onFocus={() => this.setState({ showOpt: true })} 
                                    onBlur={() => this.setState({ showOpt: false })} 
                                    onKeyUp={(e) => this.setState({ search: e.target.value })} 
                                    defaultValue={search} 
                                    type="text" 
                                    className="grafico__input" 
                                    placeholder="Procure uma moeda" 
                                />
                                {
                                    !!showOpt &&
                                    <ul className="grafico__list">
                                        {
                                            !!moedas.length ?
                                                moedas.map((v, i) =>
                                                    <li
                                                        onClick={(e) => this.setState({ showOpt: false, search: v })}
                                                        className="grafico__item"
                                                        key={i}
                                                    >
                                                        {v}
                                                    </li>
                                                ) :
                                                <li className="grafico__item">Nenhuma moeda encontrada</li>
                                        }
                                    </ul>
                                }
                            </div>
                            <p className="grafico__txt">R$</p>
                        </header>
                        <div style={{ height: "300px" }} className="tradingview-widget-container">
                            <div ref={(el) => this.graph = el} id="tradingview_e47e3"></div>
                        </div>
                    </div>
                </main>
            </div>
        )
    }
}

export default Index;