import axios from 'axios';

// Add a response interceptor
axios.interceptors.response.use((response) => {
  return response.data;
}, (error) => {
  return Promise.reject(error);
});

// Add a request interceptor
axios.interceptors.request.use((config) => {
  return {
    ...config,
    headers: {
      ...config.headers,
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${localStorage.getItem('auth_token')}`
    }
  };
}, (error) => {
  return Promise.reject(error);
});

export const get = (url) => axios({
  method: 'get',
  url,
});

export const post = (url, data) => axios({
  method: 'post',
  data,
  url,
});