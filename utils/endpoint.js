const base_urlProd = 'https://api-stg.btcbolsatestnet19732486.com';
const base_urlDev = 'http://localhost:3000';

export const ordersService = `${base_urlDev}/orders`;
export const bankService = `${base_urlDev}/bank`;
export const userService = `${base_urlDev}/users`;
export const withdrawService = `${base_urlDev}/withdraw`;
