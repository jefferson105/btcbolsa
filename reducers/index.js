import { combineReducers } from 'redux';

import orders from './orders';
import users from './users';
import bank from './bank';

const rootReducers = combineReducers({
    orders,
    users,
    bank,
});

export default rootReducers;