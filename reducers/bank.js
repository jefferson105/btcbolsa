import { FETCH_BANK_ACCOUNTS_SUCCESS } from '../actions/actionTypes';

export const initialData = {
    bankAccounts: [],
};

const bankReducer = (state = initialData, { type, data }) => {
    switch (type) {
        case FETCH_BANK_ACCOUNTS_SUCCESS:
            return {
                ...state, bankAccounts: data
            }
        default:
            return state;
    }
}

export default bankReducer;