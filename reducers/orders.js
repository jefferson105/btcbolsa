import { SET_LIVRO_ORDENS, FETCH_ALL_ORDERS_SUCCESS, FETCH_TICKER_SUCCESS, FETCH_TRADES_SUCCESS } from '../actions/actionTypes';

export const initialData = {
    livroOrdens: {
        compras: [],
        vendas: [],
        todos: []
    },
    minhasOrdens: [],
    ticker: {},
    trades: [],
};

const orderReducer = (state = initialData, { type, data }) => {
    switch (type) {
        case SET_LIVRO_ORDENS:
            return {
                ...state,
                livroOrdens: {
                    compras: data.compras,
                    vendas: data.vendas,
                    todos: data.todos
                }
            }
        case FETCH_ALL_ORDERS_SUCCESS: {
            return {
                ...state,
                minhasOrdens: data,
            }
        }
        case FETCH_TICKER_SUCCESS: {
            return {
                ...state,
                ticker: data,
            }
        }
        case FETCH_TRADES_SUCCESS: {
            return {
                ...state,
                trades: data,
            }
        }
        default:
            return state;
    }
}

export default orderReducer;