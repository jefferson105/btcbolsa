import { FETCH_USER_PROFILE_SUCCESS, FETCH_DEPOSIT_ADDRESS_SUCCESS, FETCH_MY_BALANCE } from '../actions/actionTypes';

export const initialData = {
    userProfile: {},
    depositAddress: [],
    myBalance: {},
};

const userReducer = (state = initialData, { type, data }) => {
    switch (type) {
        case FETCH_USER_PROFILE_SUCCESS:
            return {
                ...state, userProfile: data
            }
        case FETCH_DEPOSIT_ADDRESS_SUCCESS:
            return {
                ...state, depositAddress: data
            }
        case FETCH_MY_BALANCE:
            return {
                ...state, myBalance: data
            }
        default:
            return state;
    }
}

export default userReducer;