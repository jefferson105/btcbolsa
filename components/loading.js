const Loading = () => 
    <div className="overlay-loading">
        <img className="overlay-loading__img" src="/static/img/loading-circles.svg" />
    </div>

export default Loading;