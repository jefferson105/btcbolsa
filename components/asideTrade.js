import Router from "next/router";
import BoxTrade from "./boxTrade";
import socket from '../utils/socketConnection';
import { connect } from 'react-redux';
import { dispatchTicker, fetchAvailablePairs, dispatchTrades } from "../actions/orders";
import accounting from 'accounting-js';
import { getLivroOrdens } from "../actions";

class AsideTrade extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showCash: false,
            pairs: {},
        }
    }

    async componentDidMount() {
        socket.emit('ticker', 'btc/brl');
        socket.emit('trades', { pair: 'BTC/BRL' });
        socket.emit('order_book', { pair: 'BTC/BRL' }, (data) => {
            this.props.dispatch(getLivroOrdens(data));
        });
        const pairs = await this.props.dispatch(fetchAvailablePairs()) || {};
        this.setState({ pairs });

        // if (Object.keys(pairs).length > 0) {
        //     Object.keys(pairs).map((k) => {

        //     });
        // }

        socket.on('ticker', async (data) => {
            await this.props.dispatch(dispatchTicker(data));
            console.log('ticker done');
        });

        socket.on('trades', async (data) => {
            await this.props.dispatch(dispatchTrades(data));
            console.log('trades done');
        });

        socket.on('place_order', async (data) => {
            console.log(data);
        });

    }

    render() {
        const { showCash } = this.state;
        const { ticker } = this.props;

        return (
            <aside className="aside">
                <header onClick={() => Router.push("/")} style={{ padding: ".8rem" }} className="aside__header aside__container">
                    <img className="aside__logo" src="/static/img/btcbolsa.svg" />
                </header>
                <section onClick={() => this.setState({ showCash: !showCash })} style={{ cursor: "pointer" }} className="aside__container aside__container--flex">
                    <h2>BTC/BRL</h2>
                    <button className="aside__drop">
                        <img src={showCash ? "/static/img/up-arrow.svg" : "/static/img/down-arrow.svg"} />
                    </button>
                    <ul onClick={(e) => e.stopPropagation()} className={`aside__overlay ${!showCash && "aside__overlay--hide"}`}>
                        {
                            Object.keys(this.state.pairs).map((k, index) =>
                                <li key={index} onClick={() => this.setState({ showCash: false })} className={`aside__item aside__item--cursor ${this.state.pairs[k] && "aside__item--hover"}`}>
                                    <p className={`${!this.state.pairs[k] && "par-disabled"}`} style={{ textTransform: "uppercase" }}>{k.replace('_', '/')}</p>

                                    <p className={`aside__right-txt aside__blue-txt ${!this.state.pairs[k] && "par-disabled"}`}>
                                        {ticker.last
                                            ? accounting.formatMoney(ticker.last, { symbol: "R$", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })
                                            : 'R$ 0'
                                        }

                                        {ticker.var_type && <img src={ticker.var_type === 'up'
                                            ? '/static/img/up-arrow_blue.svg'
                                            : '/static/img/down-arrow_red.svg'}
                                        />}
                                    </p>
                                </li>
                            )
                        }
                    </ul>
                </section>
                <section className="aside__container">
                    <ul>
                        <li className="aside__item">
                            <p>ÚLTIMA NEG.</p>
                            <p className="aside__right-txt">
                                {ticker.last
                                    ? accounting.formatMoney(ticker.last, { symbol: "R$", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })
                                    : 'R$ 0'
                                }
                            </p>
                        </li>
                        <li className="aside__item">
                            <p>VAR 24H</p>
                            <p className={`aside__right-txt aside__${ticker.var_type === 'up' ? 'blue' : 'red'}-txt`}>
                                {ticker.var_type && <img src={
                                    ticker.var_type === 'up'
                                        ? '/static/img/up-arrow_blue.svg'
                                        : '/static/img/down-arrow_red.svg'}
                                />}
                                {ticker.var_24
                                    ? accounting.formatMoney(ticker.var_24, { symbol: "%", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })
                                    : '0'}
                            </p>
                        </li>
                        <li className="aside__item">
                            <p>MÁXIMO</p>
                            <p className="aside__right-txt">
                                {ticker.high
                                    ? accounting.formatMoney(ticker.high, { symbol: "R$", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })
                                    : 'R$ 0'
                                }
                            </p>
                        </li>
                        <li className="aside__item">
                            <p>MÍNIMO</p>
                            <p className="aside__right-txt">
                                {ticker.low
                                    ? accounting.formatMoney(ticker.low, { symbol: "R$", format: "%s %v", decimal: ",", thousand: ".", precision: 2 })
                                    : 'R$ 0'
                                }
                            </p>
                        </li>
                    </ul>
                </section>
                <section className="aside__container">
                    <h3><b>SALDO</b></h3>
                    <ul className="aside__list">
                        <li className="aside__item aside__item--hover aside__item--selected">
                            <p>Credo</p>
                            <p className="aside__right-txt">258.65</p>
                        </li>
                        <li className="aside__item aside__item--hover">
                            <p>Bitcoin</p>
                            <p className="aside__right-txt">150.50</p>
                        </li>
                    </ul>
                </section>
                <BoxTrade />
            </aside>
        )
    }
}

const mapStateToProps = (state) => ({
    ticker: state.orders.ticker,
})
export default connect(mapStateToProps)(AsideTrade);

/*
<section className={`aside__container ${!showCash && "aside__container--hide"}`}>
</section>
<button className="aside__button">Depósito/Retirada</button>
<header style={{ marginTop: "1rem" }}>
                        <button onClick={() => this.setState({ orders: true })} style={{ marginRight: ".5rem" }} className={`aside__button-border ${!!orders && "aside__button-border--active aside__button-border--white"}`}>ORDENS</button>
                        <button onClick={() => this.setState({ orders: false })} className={`aside__button-border ${!orders && "aside__button-border--active aside__button-border--white"}`}>ATIVIDADE</button>
                    </header>
<p className="aside__no-orders">Nenhum pedido realizado</p>
<p className="aside__form-item">
                                <span>Total (BTC)</span>
                                <span className="aside__right-txt">{value}</span>
                            </p>

<li className="aside__item">
                            <p className="aside__blue-txt">121.20 <img src="/static/img/sort-up.svg" /> (2.84%)</p>
                            <p className="aside__right-txt">4.459056</p>
                        </li>
*/