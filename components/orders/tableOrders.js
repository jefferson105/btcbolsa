import React from 'react';
import { Table, Select } from 'antd';
import { connect } from 'react-redux';
import { fetchAllOrders } from '../../actions/orders';
import moment from 'moment';
import { ordersState } from '../../utils/functions';

const columns = [{
    title: 'Quantidade',
    dataIndex: 'initial_amount',
    key: 'initial_amount',
    width: '15%',
},
{
    title: 'Preço Unitário',
    dataIndex: 'price_unity',
    key: 'price_unity',
    render: price_unity => `R$ ${parseFloat(price_unity).toFixed(2)}`,
    width: '10%',
}, {
    title: 'Total Executado',
    dataIndex: '',
    key: 'total_executed',
    render: ({ initial_amount = 0, avaliable_amount = 0 }) => (initial_amount - avaliable_amount).toFixed(8),
    width: '20%',
}, {
    title: 'Restante',
    dataIndex: 'avaliable_amount',
    key: 'avaliable_amount',
    width: '15%',
}, {
    title: 'Estado',
    dataIndex: 'state',
    render: state => ordersState(state),
    key: 'state',
    width: '20%',
}, {
    title: 'Data/Hora',
    dataIndex: 'time',
    key: 'time',
    width: '20%',
    render: time => `${moment(time).format('DD/MM/YYYY HH:mm')}`,
}];

const Option = Select.Option;

class TableOrders extends React.Component {
    state = {
        pagination: {},
        loading: false,
    };



    handleChange = (value) => {
        console.log(`selected ${value}`);
    }


    async componentDidMount() {
        this.setState({ loading: true });
        await this.props.dispatch(fetchAllOrders());
        this.setState({ loading: false });
    }

    render() {
        return (
            <div>
                <Select defaultValue="BTC" style={{ width: 120 }} onChange={this.handleChange}>
                    <Option value="BTC">Bitcoin</Option>
                    <Option value="LTC">Litecoin</Option>
                </Select>

                <Select defaultValue="Todos" style={{ width: 120 }} onChange={this.handleChange}>
                    <Option value="all">Todos</Option>
                    <Option value="buy">Compra</Option>
                    <Option value="sell">Venda</Option>
                </Select>

                <Select defaultValue="Todos" style={{ width: 220 }} onChange={this.handleChange}>
                    <Option value="all">Todos</Option>
                    <Option value="executed_int">Executado Totalmente</Option>
                    <Option value="executed_partially">Executado Parcialmente</Option>
                    <Option value="pending">Não executado (pendente)</Option>
                    <Option value="deleted">Cancelado</Option>
                </Select>

                <Table
                    columns={columns}
                    pagination={{ position: 'top' }}
                    expandedRowRender={record => <p style={{ margin: 0 }}>{record.identificator}</p>}
                    rowKey={record => record.identificator}
                    dataSource={this.props.minhasOrdens}
                    pagination={this.state.pagination}
                    loading={this.state.loading}
                    onChange={this.handleTableChange}
                />
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    const { minhasOrdens } = state.orders;
    return { minhasOrdens };
}
export default connect(mapStateToProps)(TableOrders);