import { connect } from "react-redux";
import scientificToDecimal from "scientific-to-decimal";
import CurrencyInput from 'react-currency-input';
import keydown, { Keys } from 'react-keydown';
import accounting from "accounting-js";
import currencyFormatter from 'currency-formatter';
import math from 'mathjs';
import socket from '../utils/socketConnection';
import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'

function setInputSelection(input, startPos, endPos) {
    if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(startPos, endPos);
    } else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', endPos);
        range.moveStart('character', startPos);
        range.select();
    }
}

function setInputPos(input, pos) {
    setInputSelection(input, pos, pos);
}

function fireKey(el,key) {
    if(document.createEventObject) {
        var eventObj = document.createEventObject();
        eventObj.keyCode = key;
        el.fireEvent("onkeydown", eventObj);
        eventObj.keyCode = key;
    }else if(document.createEvent) {
        var eventObj = document.createEvent("Events");
        eventObj.initEvent("keydown", true, true);
        eventObj.which = key;
        eventObj.keyCode = key;
        console.log(eventObj)
        el.dispatchEvent(eventObj);
    }
}

function calcMarket(qtd, ordens) {
    if (!qtd) return 0;

    ordens = ordens.filter(o => !!o);

    let totalQtd = ordens.reduce((ac, v) => {
        ac += parseFloat(v.amount);

        return ac;
    }, 0);

    console.log(qtd, totalQtd)

    if(qtd > totalQtd) return 0;

    let amount = qtd;
    let val = 0;

    for(let i = 0; i < ordens.length; i++) {
        amount -= Number(ordens[i].amount);

        if(amount < 0) {
            break;
        }

        val = parseFloat(ordens[i].price);
    }

    return val;
}

const unformatCurrency = (val) => {
  return val.replace("Ƀ", "").replace("R$","").split(".").join("").split(",").join(".");
}

const cryptoMask = createNumberMask({
  prefix: 'B ',
  suffix: '',
  includeThousandsSeparator: false,
  allowDecimal: true,
  decimalSymbol: ',',
  decimalLimit: 8,
})

const fiatMask = createNumberMask({
  prefix: 'R$ ',
  suffix: '',
  includeThousandsSeparator: true,
  allowDecimal: true,
  decimalSymbol: ',',
  thousandsSeparatorSymbol: '.',
  decimalLimit: 2,
})

class BoxTrade extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: "0.00000000",
            price: "0.00",
            buy: true,
            orders: true,
            isMercado: true,
            showModal: false,
            confirm: false,
            error: null
        }
    }

    render() {
        let { value, buy, isMercado, price, showModal, confirm, error } = this.state;
        let { livroOrdens } = this.props;

        value = parseFloat(unformatCurrency(value));
        price = parseFloat(unformatCurrency(price));

        if(isMercado) {
            let roundValue = Math.ceil(value);

            if(buy) {
                if(livroOrdens.compras.length) {
                    let val = calcMarket(value, livroOrdens.vendas)
                    price = val;
                }
            }else {
                if(livroOrdens.vendas.length) {
                    let val = calcMarket(value, livroOrdens.compras)
                    price = val;
                }
            }
        }

        let totalBruto = value * price;

        let comissao =
            buy ?
                ((value / 100) * 0.5) :
                ((totalBruto / 100) * 0.5);

        if (!parseFloat(comissao)) {
            comissao = buy ? "Ƀ 0,00000000" : "R$ 0,00";
        }else {
            comissao = currencyFormatter.format(comissao, { symbol: buy ? "Ƀ" : "R$", format: "%s %v", decimal: ",", thousand: ".", precision: buy ? 8 : 2 })
        }

        let tLiquido = currencyFormatter.format(
            Math.abs(
                buy ?
                    value - unformatCurrency(comissao) :
                    totalBruto - unformatCurrency(comissao)
            ),
            { symbol: buy ? "Ƀ" : "R$", format: "%s %v", decimal: ",", thousand: ".", precision: buy ? 8 : 2 }
        );

        return (
            <section className="aside__container">
                <div className="aside__internal-content">
                    <header className="aside__container aside__header-but">
                        <button onClick={() => this.setState({ buy: true })} className={`aside__button-border ${!!buy && "aside__button-border--active"} aside__button-border--blue`}>COMPRAR</button>
                        <button onClick={() => this.setState({ buy: false })} className={`aside__button-border ${!buy && "aside__button-border--active"} aside__button-border--red`}>VENDER</button>
                    </header>
                    <form onSubmit={(e) => e.preventDefault()} className="aside__form">
                        <div style={{ textAlign: "center", marginBottom: ".5rem" }} className="aside__buttons">
                            <button onClick={() => this.setState({ isMercado: true, price: "0" })} className={`aside__button-switch ${!!isMercado && "aside__button-switch--selected"}`}>MERCADO</button>
                            <button onClick={() => this.setState({ isMercado: false })} className={`aside__button-switch ${!isMercado && "aside__button-switch--selected"}`}>LIMITE</button>
                        </div>

                        <label className="aside__label">Quantidade</label>
                        <span className="aside__form-field">
                            <MaskedInput mask={cryptoMask} placeholder={'B 0,00000000'} className={`aside__form-input ` + error} />
                            { !!error && <span className="aside__form-error">O valor digitado é <strong>inválido</strong>.</span> }
                        </span>

                        <p style={{ marginTop: ".5rem" }}>
                            <label className="aside__label">Preço</label>
                            <span className="aside__form-field">
                                <MaskedInput mask={fiatMask} placeholder={'R$ 0,00'} disabled={!!isMercado} className={`aside__form-input ${!!isMercado && "aside__form-input--disabled"}`} />
                            </span>
                        </p>
                        <p className="aside__form-item">
                            <span>Comissão</span>
                            <span className="aside__right-txt">{comissao}</span>
                        </p>
                        <p className="aside__form-item">
                            <span>Total Líquido</span>
                            <span className="aside__right-txt">{tLiquido}</span>
                        </p>
                        <button
                            onClick={e => {
                                e.preventDefault();
                                let gt5 = totalBruto > 5;
                                this.setState({ showModal: gt5, error: !gt5 })
                            }}
                            className={`aside__form-button ${!buy && "aside__form-button--red"}`}
                        >
                            {buy ? "COMPRAR" : "VENDER"}
                        </button>
                        {!!error && <p style={{ color: "#e74c3c", textAlign: "center", marginTop: ".5rem" }}>Valor inválido</p>}
                    </form>
                </div>
                {
                    !!showModal &&
                    <div onClick={() => this.setState({ showModal: false, confirm: false })} className="screen-overlay">
                        <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
                            <header className="overlay-deposito__header">
                                <h3>Confirmação de {buy ? "COMPRA" : "VENDA"} Bitcoins</h3>
                                <button onClick={() => this.setState({ showModal: false, confirm: false })} className="overlay-deposito__close"><img src="/static/img/cancel.svg" /></button>
                            </header>
                            <div className="overlay-deposito__content">
                                {
                                    !confirm ?
                                        <React.Fragment>
                                            <p>Deseja confirmar a {buy ? "compra" : "venda"} de Bitcoins?</p>
                                            <button
                                                style={{ color: "#FFF" }}
                                                onClick={e => {
                                                    e.preventDefault();
                                                    socket.emit(
                                                        "place_order",
                                                        {
                                                            "pair": "BTC/BRL",
                                                            "amount": parseFloat(value),
                                                            "price": parseFloat(price),
                                                            "order_type": `${buy ? "buy" : "sell"} ${isMercado ? "market" : "limit"} advanced`
                                                        }
                                                    );
                                                    this.setState({ confirm: true });
                                                }}
                                                className={`aside__form-button`}
                                            >
                                                SIM
                                            </button>
                                            <button
                                                style={{ color: "#FFF" }}
                                                onClick={e => {
                                                    e.preventDefault()
                                                    this.setState({ showModal: false, confirm: false })
                                                }}
                                                className={`aside__form-button aside__form-button--red`}
                                            >
                                                NÃO
                                            </button>
                                        </React.Fragment> :
                                        <p>Ordem gravada com sucesso</p>
                                }

                            </div>
                        </div>
                    </div>
                }
            </section>
        )
    }
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

const mapStateToProps = (state) => {
    const { livroOrdens } = state.orders;
    return { livroOrdens }
}
export default connect(mapStateToProps)(BoxTrade);

//style={{ opacity: isMercado ? "1" : ".5" }}
(BoxTrade);

//style={{ opacity: isMercado ? "1" : ".5" }}
