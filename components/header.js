import Link from "next/link";
import { connect } from 'react-redux';
import { fetchUserProfile } from '../actions/users';

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            menu: this.props.page || 1
        }
    }

    componentDidMount() {
        this.props.dispatch(fetchUserProfile());
    }

    render() {
        const { menu } = this.state;

        return (
            <header className="header">
                <p className="header__button--start">
                    <a onClick={() => this.setState({ menu: 0 })} className={`header__button-bg header__button-bg--selected`}>ESCURO</a>
                    <a onClick={() => this.setState({ menu: 0 })} className={`header__button-bg`}>CLARO</a>
                </p>
                <a onClick={() => this.setState({ menu: 6 })} className={`header__button ${menu == 6 && "header__button--select"}`}>COMPRA/VENDA SIMPLES</a>
                <Link prefetch href="/">
                    <a
                        onClick={() => this.setState({ menu: 1 })}
                        className={`header__button ${menu == 1 && "header__button--select"}`}
                    >
                        LIVRO DE ORDENS
                    </a>
                </Link>
                <Link prefetch href="/carteira">
                    <a
                        onClick={() => this.setState({ menu: 2 })}
                        className={`header__button ${menu == 2 && "header__button--select"}`}
                    >
                        MEUS SALDOS
                    </a>
                </Link>
                <Link prefetch href="/graficos">
                    <a
                        onClick={() => this.setState({ menu: 3 })}
                        className={`header__button ${menu == 3 && "header__button--select"}`}
                    >
                        GRÁFICOS
                    </a>
                </Link>
                <Link prefetch href="/extratos">
                    <a 
                        onClick={() => this.setState({ menu: 4 })} 
                        className={`header__button ${menu == 4 && "header__button--select"}`}
                    >
                    EXTRATO
                    </a>
                </Link>
                <a onClick={() => this.setState({ menu: 8 })} className={`header__button ${menu == 8 && "header__button--select"}`}>MINHA CONTA</a>
            </header>
        )
    }
}

export default connect(state => state)(Header);