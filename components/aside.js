import Router from "next/router";
import CurrencyInput from 'react-currency-input';

class AsideTrade extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: "0.00",
            showCash: false,
            buy: true,
            orders: true,
            page: props.page
        }
    }

    render() {
        const { value, showCash, buy, orders, page } = this.state;

        return(
            <aside className="aside">
                <header onClick={() => Router.push("/")} style={{ padding: ".8rem" }} className="aside__header aside__container">
                    <img className="aside__logo" src="/static/img/btcbolsa.svg" />
                </header>
                <nav>
                    <ul className="aside__list">
                        <li className={`aside__item-menu ${page == 1 && "aside__item-menu--selected"}`} onClick={() => Router.push("/perfil")}>MEU PERFIL</li>
                        <li className={`aside__item-menu ${page == 20 && "aside__item-menu--selected"}`} onClick={() => Router.push("/verificacao")}>VERIFICAÇÃO DE CONTA</li>
                        <li className={`aside__item-menu ${page == 2 && "aside__item-menu--selected"}`}>PREFERÊNCIAS</li>
                        <li className={`aside__item-menu ${page == 3 && "aside__item-menu--selected"}`} onClick={() => Router.push("/seguranca")}>SEGURANÇA</li>
                        <li className={`aside__item-menu ${page == 4 && "aside__item-menu--selected"}`} onClick={() => Router.push("/pagamento")}>MINHAS CONTAS BANCÁRIAS</li>
                    </ul>
                </nav>
            </aside>
       ) 
    }
}

export default AsideTrade;

/*
<section className={`aside__container ${!showCash && "aside__container--hide"}`}>
</section>
<button className="aside__button">Depósito/Retirada</button>
<header style={{ marginTop: "1rem" }}>
                        <button onClick={() => this.setState({ orders: true })} style={{ marginRight: ".5rem" }} className={`aside__button-border ${!!orders && "aside__button-border--active aside__button-border--white"}`}>ORDENS</button>
                        <button onClick={() => this.setState({ orders: false })} className={`aside__button-border ${!orders && "aside__button-border--active aside__button-border--white"}`}>ATIVIDADE</button>
                    </header>
<p className="aside__no-orders">Nenhum pedido realizado</p>
<p className="aside__form-item">
                                <span>Total (BTC)</span>
                                <span className="aside__right-txt">{value}</span>
                            </p>
*/