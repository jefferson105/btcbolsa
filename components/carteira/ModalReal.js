import { connect } from "react-redux";

import { hideModalReal, showModalConfirmacao, setBanco } from "../../actions";

class ModalReal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            barWidth: '40%',
            typeTransfer: "ted"
        }
    }

    render() {
        const { barWidth, typeTransfer } = this.state;
        const { dispatch, modalDeposito } = this.props;

        return(
            <div onClick={() => dispatch(hideModalReal())} className="screen-overlay">
                <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
                    <header className="overlay-deposito__header">
                        <h3>Depósito em Reais</h3>
                        <button onClick={() => dispatch(hideModalReal())} className="overlay-deposito__close">
                            <img src="/static/img/cancel.svg" />
                        </button>
                    </header>
                    <div className="overlay-deposito__content">
                        <p className="overlay-deposito__label">Para qual banco deseja realizar o <strong>depósito</strong>?</p>
                        <ul className="overlay-deposito__banks"> 
                            <li onClick={() => dispatch(setBanco({ name: "Santander", slug: "santander", showTransfer: true }))} className={`overlay-deposito__banks__item overlay-deposito__banks__item--santander ${!!(modalDeposito.banco.slug == "santander") && "selected"}`}>
                                <span></span>
                            </li>
                            <li onClick={() => dispatch(setBanco({ name: "Banco do Brasil", slug: "bancobrasil", showTransfer: true }))} className={`overlay-deposito__banks__item overlay-deposito__banks__item--bb ${!!(modalDeposito.banco.slug == "bancobrasil") && "selected"}`}>
                                <span></span>
                            </li>
                            <li onClick={() => dispatch(setBanco({ name: "Outro Banco", slug: "outro", showTransfer: false }))} className={`overlay-deposito__banks__item overlay-deposito__banks__item--others ${!!(modalDeposito.banco.slug == "outro") && "selected"}`}>
                                <p>Outros</p>
                            </li>
                        </ul>
                        <div className="overlay-deposito__container-input">
                            <label className="overlay-deposito__label">Como esse depósito para o <strong>{modalDeposito.banco.name}</strong> será feito?</label>
                            {
                                !!modalDeposito.banco.showTransfer ?
                                    <div className="overlay-deposito__type">Transferência</div> :   
                                    <div className="overlay-deposito__docTed">
                                        <button onClick={() => this.setState({ typeTransfer: "ted" })} className={`overlay-deposito__buttonTrans ${typeTransfer == "ted" && "selected"}`}>TED</button>
                                        <button onClick={() => this.setState({ typeTransfer: "doc" })} className={`overlay-deposito__buttonTrans ${typeTransfer == "doc" && "selected"}`}>DOC</button>
                                    </div>
                            }
                        </div>
                        <div className="overlay-deposito__container-input">
                            <label className="overlay-deposito__label">Qual o valor do depósito?</label>
                            <input type="number" className="primary-field overlay-deposito__input overlay-deposito__value" placeholder="R$ 1.000,00" />
                            <div className="overlay-deposito__bar">
                                <div className="overlay-deposito__bar__container">
                                    <span className="overlay-deposito__bar__limit">Limite</span>
                                    <span className="overlay-deposito__bar__value">R$ 2.000,00</span>
                                </div>
                                <div className="overlay-deposito__bar__current" style={{ width: barWidth }}></div>
                                <div className="overlay-deposito__bar__help">!</div>
                            </div>
                            <a href="#" className="overlay-deposito__bar__increase">Deseja aumentar seu limite?</a>
                        </div>
                    </div>
                    <footer className="overlay-deposito__footer">
                        <button onClick={() => dispatch(hideModalReal())} className="primary-button primary-button--text">Cancelar</button>
                        <button onClick={() => { 
                            dispatch(hideModalReal());
                            dispatch(showModalConfirmacao());
                        }} className="primary-button primary-button--continue">Continuar</button>
                    </footer>
                </div>
            </div>
        )
    }
}

export default connect(state => state)(ModalReal);