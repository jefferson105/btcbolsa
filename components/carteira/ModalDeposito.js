import { connect } from "react-redux";

const ModalDeposito = ({ dispatch }) =>
    <div onClick={() => this.setState({ showDeposito: false })} className="screen-overlay">
        <div onClick={(e) => e.stopPropagation()} className="overlay-deposito">
            <header className="overlay-deposito__header">
                <h3>Depósito</h3>
                <button onClick={() => this.setState({ showDeposito: false })} className="overlay-deposito__close"><img src="/static/img/cancel.svg" /></button>
            </header>
            <div className="overlay-deposito__content">
                <div className="overlay-deposito__container-input">
                    <label className="overlay-deposito__label">Endereço da Carteira</label>
                    <p className="overlay-deposito__wallet">
                        <span>1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX</span>
                        <img className="overlay-deposito__copy" src="/static/img/copy.svg" />
                    </p>
                </div>
                <div className="overlay-deposito__info">
                    <div className="overlay-deposito__container-txt">
                        <h4 className="overlay-deposito__title">Your Limits</h4>
                        <p style={{ marginBottom: "1rem" }}><b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit. Maecenas mi diam, pretium a est et, facilisis rhoncus felis.</p>

                        <h4 className="overlay-deposito__title">Your Limits</h4>
                        <p style={{ marginBottom: "1rem" }}><b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit. Maecenas mi diam.</p>
                    </div>
                    <figure className="overlay-deposito__fig-qr-code">
                        <img className="overlay-deposito__qr-code" src="/static/img/qr-code.png" />
                    </figure>
                </div>
            </div>
        </div>
    </div>

export default connect(state => state)(ModalDeposito);